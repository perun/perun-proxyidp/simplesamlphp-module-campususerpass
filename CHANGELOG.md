# [2.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-campususerpass/compare/v1.0.1...v2.0.0) (2022-11-07)


### Bug Fixes

* **deps:** update dependency php to 8.1 ([3f47962](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-campususerpass/commit/3f47962afbfeff11d4db2345e09e597f1708db84))


### BREAKING CHANGES

* **deps:** PHP ^8.1 is required

## [1.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-campususerpass/compare/v1.0.0...v1.0.1) (2022-09-28)


### Bug Fixes

* first release from GitLab ([4528308](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-campususerpass/commit/45283085cb2fd8f0a1f55b82338c2a216722a16d))

# 1.0.0 (2022-07-14)


### Features

* initial release ([1ecf4a8](https://github.com/CESNET/simplesamlphp-module-campususerpass/commit/1ecf4a81038abc3b4cf3cbec75b6379ac880b453))
